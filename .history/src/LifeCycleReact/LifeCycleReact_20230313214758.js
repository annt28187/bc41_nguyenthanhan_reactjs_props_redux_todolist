import React, { Component } from 'react';
import ChildComponent from './ChildComponent';

export default class LifeCycleReact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 1,
    };
  }

  render() {
    return (
      <div>
        <ChildComponent />
      </div>
    );
  }
}
