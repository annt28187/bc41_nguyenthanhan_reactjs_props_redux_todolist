import ToDoList from './JSS_StyledComponents/BaiTapStyledComponent/ToDoList/ToDoList';
import DenoJSS from './JSS_StyledComponents/DemoJSS/DenoJSS';
import { DemoTheme } from './JSS_StyledComponents/Themes/DemoTheme';

function App() {
  return (
    <div>
      {/* <DenoJSS /> */}
      {/* <DemoTheme /> */}
      <ToDoList />
    </div>
  );
}

export default App;
