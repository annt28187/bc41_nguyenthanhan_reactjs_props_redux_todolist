import DenoJSS from './JSS_StyledComponents/DemoJSS/DenoJSS';
import DemoTheme from './JSS_StyledComponents/Themes/DemoTheme';

function App() {
  return (
    <div>
      {/* <DenoJSS /> */}
      <DemoTheme />
    </div>
  );
}

export default App;
