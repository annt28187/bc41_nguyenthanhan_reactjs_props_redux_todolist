import logo from './logo.svg';
import './App.css';
import DenoJSS from './JSS_StyledComponents/DemoJSS/DenoJSS';

function App() {
  return (
    <div>
      <DenoJSS />
    </div>
  );
}

export default App;
