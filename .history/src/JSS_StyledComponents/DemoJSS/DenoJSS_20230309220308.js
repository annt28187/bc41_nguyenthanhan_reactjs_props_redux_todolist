import React, { Component } from 'react';
import { Button } from './../Components/Button';

export default class DenoJSS extends Component {
  render() {
    return (
      <div className="container-fluid mt-5 btn__styled">
        <Button> Click me! </Button>
      </div>
    );
  }
}
