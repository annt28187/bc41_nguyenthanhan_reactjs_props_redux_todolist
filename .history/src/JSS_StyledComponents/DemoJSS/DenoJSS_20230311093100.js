import React, { Component } from 'react';
import { Button } from './../Components/Button';

export default class DenoJSS extends Component {
  render() {
    return (
      <div className="container-fluid mt-5">
        <Button fontSize> Click me! </Button>
        <br />
        <Button className="mt-2" primary>
          Nhấn vào đây
        </Button>
      </div>
    );
  }
}
