import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

export const DemoTheme = (propsComponent) => {
  const configTheme = {
    darkColor: '#000',
    blueColor: '#3333CC',
  };

  const DivStyle = styled.div`
    color: ${(props) => props.theme.darkColor};
    padding: 5%;
    background: ${(props) => props.theme.blueColor};
  `;

  return (
    <ThemeProvider theme={configTheme}>
      <div className={DivStyle}>
        <h2>DemoTheme</h2>
      </div>
    </ThemeProvider>
  );
};
