import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

export const DemoTheme = (propsComponent) => {
  const configDarkTheme = {
    background: '#000',
    color: '#ffff',
  };

  const configLightTheme = {
    background: '#000',
    color: '#ffff',
  };

  const DivStyle = styled.div`
    color: ${(props) => props.theme.color};
    padding: 5%;
    background-color: ${(props) => props.theme.background};
  `;

  return (
    <ThemeProvider theme={configDarkTheme}>
      <DivStyle>
        <h2>DemoTheme</h2>
      </DivStyle>
    </ThemeProvider>
  );
};
