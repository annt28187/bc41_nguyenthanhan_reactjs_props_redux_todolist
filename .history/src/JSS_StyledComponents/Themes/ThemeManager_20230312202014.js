import { ToDoListDarkTheme } from './ToDoListDarkTheme';
import { ToDoListLightTheme } from './ToDoListLightTheme';
import { ToDoListPrimaryTheme } from './ToDoListPrimaryTheme';

export const themeArr = [ToDoListDarkTheme, ToDoListLightTheme, ToDoListPrimaryTheme];
