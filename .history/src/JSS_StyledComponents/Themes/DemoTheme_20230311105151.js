import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

export const DemoTheme = (propsComponent) => {
  const configTheme = {
    darkColor: '#000',
    whiteColor: '#ffff',
    blueColor: '#3333CC',
  };

  const DivStyle = styled.div`
    color: ${(props) => props.theme.whiteColor};
    padding: 5%;
    background-color: ${(props) => props.theme.blueColor};
  `;

  return (
    <ThemeProvider theme={configTheme}>
      <DivStyle>
        <h2>DemoTheme</h2>
      </DivStyle>
    </ThemeProvider>
  );
};
