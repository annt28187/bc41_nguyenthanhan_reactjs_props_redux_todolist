import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

export const DemoTheme = (propsComponent) => {
  const configTheme = {
    darkColor: '#000',
    blueColor: '#3333CC',
  };

  const DivStyle = styled.div`
    color: ${(props) => props.theme.darkColor};
  `;

  return (
    <ThemeProvider theme={configTheme}>
      <div>
        <h2>DemoTheme</h2>
      </div>
    </ThemeProvider>
  );
};
