import styled from 'styled-components';

export const Button = styled.button`
  color: #fff;
  background: linear-gradient(#f74c0b, #ec4736);
  border: none;
  border-radius: 0.5rem;
  font-weight: bold;
  padding: 1rem;
  opacity: 1;
  &:hover {
    opacity: 0.7;
    transition: all 0.5s;
  }
  &.btn__styled {
    font-size: 32px;
  }
`;
