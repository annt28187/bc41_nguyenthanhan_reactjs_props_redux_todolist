import styled from 'styled-components';

export const Link = ({ className, children, ...restProps }) => {
  return <a className={className}>{children}</a>;
};
