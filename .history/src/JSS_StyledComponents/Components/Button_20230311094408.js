import styled from 'styled-components';

export const Button = styled.button`
  color: #fff;
  background: ${(props) =>
    props.primary
      ? 'linear-gradient(to bottom right, #F9C893, #EFBB9B)'
      : 'linear-gradient(to bottom right, #8D9DEF, #74D5F4)'};
  border: none;
  border-radius: 0.5rem;
  font-size: ${(props) => (props.fontSize ? '2rem' : '1rem')};
  color: ${(props) => (props.fontSize ? 'red' : 'blue')};
  font-weight: bold;
  padding: 1rem;
  opacity: 1;
  &:hover {
    opacity: 0.7;
    transition: all 0.5s;
  }
`;

export const SmallButton = styled(Button)`
  background-color: orange;
  font-size: 0.5rem;
  padding: 0.5rem;
`;
