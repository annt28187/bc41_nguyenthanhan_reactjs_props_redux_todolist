import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';
import { ToDoListDarkTheme } from './../../Themes/ToDoListDarkTheme';
import { Dropdown } from './../../Components/Dropdown';
import { Heading3 } from './../../Components/Heading';

export default class ToDoList extends Component {
  render() {
    return (
      <ThemeProvider theme={ToDoListDarkTheme}>
        <Container className="w-50">
          <Dropdown>
            <option value="1">Dark Theme</option>
            <option value="2">Light Theme</option>
            <option value="3">Primary Theme</option>
          </Dropdown>
          <Heading3>To Do List</Heading3>
        </Container>
      </ThemeProvider>
    );
  }
}
