import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';
import { ToDoListDarkTheme } from './../../Themes/ToDoListDarkTheme';

export default class ToDoList extends Component {
  render() {
    return (
      <ThemeProvider theme={ToDoListDarkTheme}>
        <Container className="w-50">To Do List</Container>
      </ThemeProvider>
    );
  }
}
