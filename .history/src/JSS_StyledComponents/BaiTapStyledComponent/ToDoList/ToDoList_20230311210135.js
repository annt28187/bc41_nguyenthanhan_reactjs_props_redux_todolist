import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';

export default class ToDoList extends Component {
  render() {
    return (
      <div>
        <Container>To Do List</Container>
      </div>
    );
  }
}
