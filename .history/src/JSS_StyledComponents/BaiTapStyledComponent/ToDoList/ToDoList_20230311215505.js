import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';
import { ToDoListDarkTheme } from './../../Themes/ToDoListDarkTheme';
import { Dropdown } from './../../Components/Dropdown';
import { Heading3 } from './../../Components/Heading';
import { TextField } from '../../Components/TextField';
import { Button } from '../../Components/Button';
import { Table, Th, Tr, Td, Thead, Tbody } from './../../Components/Table';

export default class ToDoList extends Component {
  render() {
    return (
      <ThemeProvider theme={ToDoListDarkTheme}>
        <Container className="w-50">
          <Dropdown>
            <option value="1">Dark Theme</option>
            <option value="2">Light Theme</option>
            <option value="3">Primary Theme</option>
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField label="task name" className="w-50"></TextField>
          <Button className="ml-2">
            <i class="fa fa-plus"></i>Add task
          </Button>
          <Button className="ml-2">
            <i class="fa fa-upload"></i>Update task
          </Button>
          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>
              <Tr>
                <Th>Task name</Th>
                <Th className="text-right">
                  <Button>
                    <i class="fa fa-edit"></i>
                  </Button>
                  <Button>
                    <i class="fa fa-check"></i>
                  </Button>
                  <Button>
                    <i class="fa fa-trash"></i>
                  </Button>
                </Th>
              </Tr>
              <Tr>
                <Th>Task name</Th>
                <Th className="text-right">
                  <Button>
                    <i class="fa fa-edit"></i>
                  </Button>
                  <Button>
                    <i class="fa fa-check"></i>
                  </Button>
                  <Button>
                    <i class="fa fa-trash"></i>
                  </Button>
                </Th>
              </Tr>
            </Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}
