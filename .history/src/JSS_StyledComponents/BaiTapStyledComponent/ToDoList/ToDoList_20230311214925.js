import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';
import { ToDoListDarkTheme } from './../../Themes/ToDoListDarkTheme';
import { Dropdown } from './../../Components/Dropdown';
import { Heading3 } from './../../Components/Heading';
import { TextField } from '../../Components/TextField';
import { Button } from '../../Components/Button';
import { Table } from './../../Components/Table';

export default class ToDoList extends Component {
  render() {
    return (
      <ThemeProvider theme={ToDoListDarkTheme}>
        <Container className="w-50">
          <Dropdown>
            <option value="1">Dark Theme</option>
            <option value="2">Light Theme</option>
            <option value="3">Primary Theme</option>
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField label="task name" className="w-50"></TextField>
          <Button className="ml-2">
            <i class="fa fa-plus"></i>Add task
          </Button>
          <Button className="ml-2">
            <i class="fa fa-upload"></i>Update task
          </Button>
          <hr />
          <Heading3>Task to do</Heading3>
        </Container>
      </ThemeProvider>
    );
  }
}
