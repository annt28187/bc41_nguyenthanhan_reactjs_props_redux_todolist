import React, { Component } from 'react';
import { Container } from '../../Containers/Container';
import { ThemeProvider } from 'styled-components';
import { ToDoListDarkTheme } from './../../Themes/ToDoListDarkTheme';
import { Dropdown } from './../../Components/Dropdown';
import { Heading3 } from './../../Components/Heading';
import { TextField } from '../../Components/TextField';
import { Button } from '../../Components/Button';
import { Table, Th, Tr, Td, Thead, Tbody } from './../../Components/Table';
import { connect } from 'react-redux';
import { themeArr } from '../../Themes/ThemeManager';
import { changeThemeAction } from '../redux/actions/ToDoListAction';

class ToDoList extends Component {
  state = {
    taskName: '',
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-1">
                <i class="fa fa-edit"></i>
              </Button>
              <Button className="ml-1">
                <i class="fa fa-check"></i>
              </Button>
              <Button className="ml-1">
                <i class="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-1">
                <i class="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return themeArr.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField onChange={(e)=>{
            this.setState({
              taskName: e.value.target;
            })
          }} label="task name" className="w-50"></TextField>
          <Button className="ml-2" onClick={() => {}}>
            <i class="fa fa-plus"></i>Add task
          </Button>
          <Button className="ml-2">
            <i class="fa fa-upload"></i>Update task
          </Button>
          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
  };
};

export default connect(mapStateToProps)(ToDoList);
