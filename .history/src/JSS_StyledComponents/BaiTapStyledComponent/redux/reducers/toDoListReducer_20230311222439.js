import { ToDoListDarkTheme } from '../../../Themes/ToDoListDarkTheme';

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
