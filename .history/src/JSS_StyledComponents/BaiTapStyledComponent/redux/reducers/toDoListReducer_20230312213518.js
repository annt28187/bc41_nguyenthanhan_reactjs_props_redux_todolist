import { themeArr } from '../../../Themes/ThemeManager';
import { ToDoListDarkTheme } from '../../../Themes/ToDoListDarkTheme';
import { ADD_TASK, CHANGE_THEME } from '../types/ToDoListTypes';

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 'task-1', taskName: 'task 1', done: true },
    { id: 'task-2', taskName: 'task 2', done: false },
    { id: 'task-3', taskName: 'task 3', done: true },
    { id: 'task-4', taskName: 'task 4', done: false },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      if (action.newTask.name.trim() === '') {
        alert('Task name is required!');
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.taskName === action.newTask.taskName);

      state.taskList = taskListUpdate;

      return { ...state };
    }
    case CHANGE_THEME: {
      let theme = themeArr.find((theme) => theme.id == action.themeId);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};
