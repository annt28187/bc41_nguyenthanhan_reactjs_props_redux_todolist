import { themeArr } from '../../../Themes/ThemeManager';
import { ToDoListDarkTheme } from '../../../Themes/ToDoListDarkTheme';
import {
  ADD_TASK,
  DONE_TASK,
  DELETE_TASK,
  CHANGE_THEME,
  EDIT_TASK,
  UPDATE_TASK,
} from '../types/ToDoListTypes';

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 'task-1', taskName: 'task 1', done: true },
    { id: 'task-2', taskName: 'task 2', done: false },
    { id: 'task-3', taskName: 'task 3', done: true },
    { id: 'task-4', taskName: 'task 4', done: false },
  ],
  taskEdit: { id: 'task-1', taskName: 'task 1', done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      if (action.newTask.taskName.trim() === '') {
        alert('Task name is required!');
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.taskName === action.newTask.taskName);
      if (index !== -1) {
        alert('Task name already exists!');
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      state.taskList = taskListUpdate;

      return { ...state };
    }
    case CHANGE_THEME: {
      let theme = themeArr.find((theme) => theme.id == action.themeId);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter((task) => task.id !== action.taskId);
      return { ...state, taskList: taskListUpdate };
    }

    case EDIT_TASK: {
      return { ...state, taskEdit: action.task };
    }
    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === state.taskEdit.id);
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskEdit = taskListUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
