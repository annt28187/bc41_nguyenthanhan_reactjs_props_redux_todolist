import { CHANGE_THEME } from '../types/ToDoListTypes';

export const changeThemeAction = (themeId) => ({
  type: CHANGE_THEME,
  themeId,
});
