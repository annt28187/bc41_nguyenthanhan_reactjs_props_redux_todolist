import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK } from '../types/ToDoListTypes';

export const addTaskAction = (newTask) => ({
  type: ADD_TASK,
  newTask,
});

export const changeThemeAction = (themeId) => ({
  type: CHANGE_THEME,
  themeId,
});

export const doneTaskAction = (taskId) => ({
  type: DONE_TASK,
  taskId,
});

export const deleteTaskAction = (taskId) => ({
  type: DELETE_TASK,
  taskId,
});

export const editTaskAction = (taskId) => ({
  type: EDIT_TASK,
  taskId,
});

// {return{}} = ({})
