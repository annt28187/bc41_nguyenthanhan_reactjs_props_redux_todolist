import { ADD_TASK, CHANGE_THEME } from '../types/ToDoListTypes';

export const addTaskAction = (newTask) => ({
  type: ADD_TASK,
  newTask,
});

export const changeThemeAction = (themeId) => ({
  type: CHANGE_THEME,
  themeId,
});
