import ToDoList from './JSS_StyledComponents/BaiTapStyledComponent/ToDoList/ToDoList';
import LifeCycleReact from './LifeCycleReact/LifeCycleReact';

function App() {
  return (
    <div>
      {/* <DenoJSS /> */}
      {/* <DemoTheme /> */}
      {/* <ToDoList /> */}
      <LifeCycleReact />
    </div>
  );
}

export default App;
